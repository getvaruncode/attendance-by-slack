
# Attendance-By-Slack

**Attendance-by-Slack** was created to manage the attendance of a school robotics team. In the past, paper spreadsheets and id cards were used. However those papers and barcodes were easily lost, attendance was subject to abuse by students, team attendance records were incomplete and therefore sometimes not useful to team leadership.

Slack was already used as the team�s primary communication tool. Attendance-by-Slack added the capability for team members to sign in and sign out within Slack. Team leadership approves or rejects these requests within the same Slack Workspace. 

Feel free to use Attendance-by-Slack for any of your attendance needs!

Contact me at trombonevarun@gmail.com for suggestions and improvments

### How it works:
##### Team Members

Each team member is expected to sign in through a direct message with the AttendanceBot using the slash command /signin, as shown below. After a successful sign in request, the team member receives an acknowledgement, and is prompted to sign out or cancel their sign in, as shown below.

![slashcommand](https://bitbucket.org/getvaruncode/attendance-by-slack/raw/d21d8042f931836bfb2268ee5f0c1c803e0e81a3/screenshots/1-memberSignIn.png)

After the work for the day has been completed, the team member has to click the sign out button. After sign out, the member will receive the following message with the minutes logged for the day. 

![minslogged](https://bitbucket.org/getvaruncode/attendance-by-slack/raw/d21d8042f931836bfb2268ee5f0c1c803e0e81a3/screenshots/4-memberSignOutResponseApprove.png)

##### Leadership
Meanwhile, a team member sign in will result in the following message in a private channel for leadership. 

![leadslash](https://bitbucket.org/getvaruncode/attendance-by-slack/raw/d21d8042f931836bfb2268ee5f0c1c803e0e81a3/screenshots/2-memberSignInResponse.png)

Any leader can approve or reject this sign in. 

![leadapprove](https://bitbucket.org/getvaruncode/attendance-by-slack/raw/d21d8042f931836bfb2268ee5f0c1c803e0e81a3/screenshots/3-memberSignInApproved.png)
	
A leader can also sign out all remaining users

![](https://bitbucket.org/getvaruncode/attendance-by-slack/raw/d21d8042f931836bfb2268ee5f0c1c803e0e81a3/screenshots/7-leadershipSignOutAllResponse.png)

##### Display Summary of all hours logged
To receive a summary of total hours logged by each team member, anybody in the leadership channel can use the slash command "/get-summary"

![getSum](https://bitbucket.org/getvaruncode/attendance-by-slack/raw/d21d8042f931836bfb2268ee5f0c1c803e0e81a3/screenshots/8-leadershipGetSummary.png)

#### Setup
##### Slack Setup
[Click here to see](https://www.youtube.com/watch?v=rX_J3v7VCOM) how to setup your Slack Workspace to implement Attendance-by-Slack!
Alternatively, follow these instructions: 

1. Use your Administrator account to log into your Slack Workspace


2. Create a **private channel** called *leadership*


3. Get its **channel id** from the channel's details; note it as LEADERSHIP_CHANNEL_ID


4. Go to **Settings & Administration**, then to **Workspace Settings**


5. Go to **Menu**, then to **API** -- it will open in a new tab


6. At middle of the screen, click **Create an App**


7. Create the app *From the Manifest*, and choose your workspace when prompted


8. Copy and Paste the Sample App Manifest [from the repository](https://bitbucket.org/getvaruncode/attendance-by-slack/src/master/slackSetup/sampleManifest.yml)


9. Click **Create**, then **Install to Workspace** 


10. Under *Features* on the left side panel, click **App Home**


11. Scroll down, and check the *Allow users to send Slash commands and messages from the messages tab* box


12. Click the blue oval on the top of the screen and leads you to the new Slack App Home


13. On the left side panel, click the **Incoming Webhooks** tab


14. Obtain the webhook (you may have to scroll down), note it as LEADERSHIP_WEBHOOK


15. On the left side panel, click the **OAuths and Permissions** tab


16. Obtain the Bot User OAuth Token (you may have to scroll down), note it as BOT_TOKEN. It should begin with "xoxb"


17. On the left side panel, click **Basic Information**


18. Reveal the **Signing Secret** and copy it, note it as SLACK_SIGNING_SECRET


19. Go back to your workspace and open a direct message with the **AttendanceAppDemo** Bot


20. Under Conversation Details, obtain the bot's **Member ID**, note it as BOT_ID



21. The following variables will be used in the Heroku configuration later: LEADERSHIP_CHANNEL_ID, SLACK_SIGNING_SECRET, BOT_TOKEN, BOT_ID, LEADERSHIP_WEBHOOK


##### Heroku setup
Use Heroku to host the attendance server. First create the application *temp-attend* using the following command
```
$ heroku create -a temp-attend

```
If *temp-attend* is  not available, use a unique name that heroku accepts. In Slack Configuration with Heroku Settings (below), replace *temp-attend* with the unique name you chose.
##### Mongo DB setup
You need a MongoDB backend which is required by the attendance application. You could use Mongo Atlas at mongodb.com to create it. 
```
Mongo environment variables needed for setup: HOST, USERNAME, PASSWORD, DB_NAME
```
##### Heroku environment setup
Setup these environment variables in the Heroku application created above

![herokuvars](https://bitbucket.org/getvaruncode/attendance-by-slack/raw/d21d8042f931836bfb2268ee5f0c1c803e0e81a3/screenshots/9-herokuDetails.png)

##### Application Deployment
Deploy Attendance-By-Slack to this heroku environment
```
$ git clone https://bitbucket.org/getvaruncode/attendance-by-slack.git
$ cd attendance-by-slack
$ heroku git:remote -a temp-attend
$ git push heroku master
```
##### Configure Slack with Heroku settings
- In the App Manifest, replace all instances of *https://dummyurl.com* with your  *https://temp-attend.herokuapp.com/*. 
- You need to change the **url** for all the *slash commands*, and the **request_url** for *interactivity*

##### Your AttendanceBot is now ready for use!
