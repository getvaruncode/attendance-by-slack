let express = require('express');
let app = express();

const slackInteractionListener = require('./my_modules/InteractiveMessageHandler');

//
// Plug the slackListener as middleware. This needs to be added before the body parsing functions below
// Not sure why this order is necessary, but slack has documented that express throws a 500 response back
// if not declared in this order
//
app.use('/slack/interactions', slackInteractionListener);

//
// req.rawBody is needed for verifying slack signatures. This middleware function retrieves
// the raw body of the request
const rawBodyBuffer = (req, res, buf, encoding) => {
  if (buf && buf.length) {
    req.rawBody = buf.toString(encoding || 'utf8');
  }
};

//
// Add Middleware to support urlencoded, json with the rawBodyBuffer middleware function
//
app.use(express.json({ verify: rawBodyBuffer }));
app.use(express.urlencoded({ verify: rawBodyBuffer, extended: true }));

//after user uses any slash command in #general
app.post('/slack/slashCommand', require("./my_modules/SlashCommandHandler"));

// Dummy URL
app.use('/ping', (req, res) => {
	console.log("Body is " + JSON.stringify(req.body));
	console.log("Headers are " + JSON.stringify(req.headers));
    res.json("You got through!");
});

// Get ready to start express server
//
let portNum = process.env.PORT || 8000;
let ip = process.env.ip;

// Start Express Server
//
let server = app.listen(portNum, ip, async () => {
	// Connect to Database
	//
	let connectDB = require('./lib/mongoEnv');
	await connectDB();

	let host = server.address().address;
	let port = server.address().port;
	console.log('Attendance Application listening at http://%s:%s\n', host, port);
});