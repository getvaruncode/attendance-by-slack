let mongoose = require('mongoose');

let MONGO_HOST = process.env.MONGO_HOST || 'localhost';
let MONGO_DB   = process.env.MONGO_DB || 'ChessClients';
let MONGO_USER = process.env.MONGO_USER || 'chessadmin';
let MONGO_PWD  = process.env.MONGO_PWD || 'password';

let MONGO_URL = 'mongodb+srv://' + MONGO_HOST + "/" + MONGO_DB;

let connectDB = async () => {
	let options = {
		user: MONGO_USER,
		pass: MONGO_PWD,
		useNewUrlParser: true,
		useUnifiedTopology: true,
		retryWrites: true,
		writeConcern: 'majority',      
		useCreateIndex: true,
	};
	try {
		await mongoose.connect(MONGO_URL, options);
		console.log("Connected to " + MONGO_URL + " DB at " + new Date().toLocaleString());
	} catch (err) {
		console.error("DB connection failed: " + err.message);
		process.exit(1);
	}
}

module.exports = connectDB;





