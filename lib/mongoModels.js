var mongoose = require('mongoose');

var attendanceSchema = new mongoose.Schema({
	user: {
		name: String,
		user_id: String
	},
	signIn: {
		time: Date,
		status: String,
		approvedBy: {
			id: String,
			name: String
		},
		approveTime: Date
	},
	signOut: {
		time: Date,
		status: String
	},
	totalMinutesWorked: Number,
	responseURL: String
});

var attendanceModel = mongoose.model("attendances", attendanceSchema);

module.exports.ATTENDANCE_MODEL = attendanceModel;
