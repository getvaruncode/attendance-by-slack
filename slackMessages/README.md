# Attendance-By-Slack

**Attendance-by-Slack** was created to manage the attendance of a school robotics team. In the past, paper spreadsheets and id cards were used. However those papers and barcodes were easily lost, attendance was subject to abuse by students, team attendance records were incomplete and therefore sometimes not useful to team leadership.

Slack was already used as the team�s primary communication tool. Attendance-by-Slack added the capability for team members to sign in and sign out within Slack. Team leadership approves or rejects these requests within the same Slack Workspace. 

Feel free to use Attendance-by-Slack for any of your attendance needs!

Contact me at trombonevarun@gmail.com for suggestions and improvments

### How it works:
##### Team Members
- Each team member is expected to sign in through a direct message with the AttendanceBot using the slash command /signin, as shown below

	![slashcommand](https://i.imgur.com/4eETg8v.jpg?2)

- After a successful sign in request, the team member receives an acknowledgement, and is prompted to sign out or cancel their sign in, as shown below.

	![slashres](https://i.imgur.com/p9MVz2c.jpg?1)

- After the work for the day has been completed, the team member has to click the sign out button. After sign out, the member will receive the following message with the minutes logged for the day. 

	![minslogged](https://i.imgur.com/rHFIgJY.jpg?1)

##### Leadership
- Meanwhile, a team member sign in will result in the following message in a private channel for leadership. 

	![leadslash](https://i.imgur.com/YHlFrBP.jpg?1)

- Any leader can approve or reject this sign in. 

	![leadapprove](https://i.imgur.com/Kwygpbt.jpg?1)

##### Display Summary of all hours logged
- To recieve a summary of total hours logged by each team member, anybody in the leadership channel can use the slash command "/get-summary"

	![getSum](https://i.imgur.com/rusSz15.jpg?3)

#### Setup
##### Slack Setup
[Click here to see](https://www.youtube.com/watch?v=ip0ybdaVclk&feature=youtu.be) how to setup your Slack Workspace to implement Attendance-by-Slack!
Alternatively, follow these instructions: 

1. Use your Administrator account to log into your Slack Workspace


2. Create a **private channel** called *attendance*


3. Get its **channel id** from the channel's url; note it as LEADERSHIP_CHANNEL_ID

	a. https://myworkspace.slack.com/messages/**CK0GSLYSW**/


4. Go to **Administration**, then to **Workspace Settings**


5. Go to **Menu**, then to **API** -- it will open in a new tab


6. At the top right corner of the screen, click **Your Apps**


7. Click **Create an App** and call it *AttendanceApp.*


8. Deploy it in your workspace


9. Retrieve **Verification Token** (you may have to scroll down), note it as SLACK_APP_TOKEN


10. On the left side panel, click **Bot Users**


11. Click **Add a Bot User**. Call it *attendancebot* on both the display name and default username


12. On the left side panel, click **Install App**, then click **Install App to Workspace**


13. Slack may change the name of your bot to *attendancebot2*, take note of it if this happens


14. Obtain **Bot User OAuth Access Token**; note it as BOT_TOKEN


15. In a separate tab, go to [https://slack.com/api/users.list?token={BOT_TOKEN}&pretty=1](https://slack.com/api/users.list?token=%7bBOT_TOKEN%7d&pretty=1), replacing {BOT_TOKEN} with the BOT_TOKEN obtained in step 14


16. Search for *attendancebot* (Or *attendancebot2*, if slack changed the bot's name) and obtain its **id**, note it as **BOT_ID**


17. On the left side panel, click **Slash Commands**


18. **Create** 3 slash commands -- "*/signin*", "*/sign-all-out*", "*/get-summary*". For the **Request URL**, use the dummy url **[http://dummyurl.com](http://dummyurl.com/)**


19. At the top of the page on a green banner, **Reinstall the App**


20. On the left side panel, click **Interactive Components**


21. Enable interactive components with the toggle on the right side of the page, and change the **Request URL** to the same dummy url


22. On the left side panel, click **Incoming Webhooks**


23. Activate Incoming Webhooks with the toggle on the right, and **Add a new Webhook to Workspace** (You may have to scroll down)


24. **Post to** the private channel *attendance* and note the **Webhook** **URL** as LEADERSHIP_WEBHOOK


25. **Reinstall** the app once again


26. The following variables will be used in the Heroku configuration later: LEADERSHIP_CHANNEL_ID, SLACK_APP_TOKEN, BOT_TOKEN, BOT_ID, LEADERSHIP_WEBHOOK


##### Heroku setup
Use Heroku to host the attendance server. First create the application *temp-attend* using the following command
```
$ heroku create -a temp-attend

```
If *temp-attend* is  not available, use a unique name that heroku accepts. In Slack Configuration with Heroku Settings (below), replace *temp-attend* with the unique name you chose.
##### Mongo DB setup
You need a MongoDB backend which is required by the attendance application. You could use mlab.com to create it. 
```
Mongo environment variables needed for setup: HOST, PORT, USERNAME, PASSWORD, DB_NAME
```
##### Heroku envionment setup
Setup these environment variables in the Heroku application created above

![herokuvars](https://i.imgur.com/kwipsuG.png)

##### Application Deployment
Deploy Attendance-By-Slack to this heroku environment
```
$ git clone https://bitbucket.org/getvaruncode/attendance-by-slack.git
$ cd attendance-by-slack
$ heroku git:remote -a temp-attend
$ git push heroku master
```
##### Configure Slack with Heroku settings
- Change the request url for all three slash commands to https://temp-attend.herokuapp.com/slack/slashCommand

	![slashReq](https://i.imgur.com/UVmDSAV.png?2)

- Change the request url for all interactive components to https://temp-attend.herokuapp.com/slack/buttonPressed

	![interactiveReq](https://i.imgur.com/48SkKSf.png?2)

##### Your AttendanceBot is now ready for use!
