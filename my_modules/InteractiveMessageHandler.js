/**
 * Summary. This file contains the main function that handles a slack action
 * in an interactive message. This is a new file that uses the 
 * @slack/interative-messages library
 *
 * Description. The actions here are expected to handle all button presses in
 * a slack message for the Attendance App
 *
 * @link   
 * @file   
 * @author Varun Bhajekar
 * @since  1.0.1
 */
 
const attendanceModel = require('../lib/mongoModels.js').ATTENDANCE_MODEL;
const { renderTemplateFile } = require('template-file');
const { IncomingWebhook } = require('@slack/webhook');
const webhook = new IncomingWebhook(process.env.LEADERSHIP_WEBHOOK);
const got = require('got');
const to  = require('await-to-js').default; 

const { createMessageAdapter } = require('@slack/interactive-messages');
const slackSigningSecret = process.env.SLACK_SIGNING_SECRET;
const slackInteractions = createMessageAdapter(slackSigningSecret);

/**
 * Main Interactive Message handler that handles all action messages from slack
 *
 * @param {Object} payload The parsed interactive message payload in JSON.
 * @param {function} respond Function used to send the response back to slack client.
 * @return {null} Nothing.
 *
 */
slackInteractions.action({ type: 'button' }, async (payload, respond) => {
  console.log('payload is ', payload);

  let isInValid = isRequestInvalid(payload);
	if (isInValid) {
		// Houston we have a problem
		console.log("Couldn't process Message: " + isInValid);
		respond("{'text': 'Invalid request received'}");
	} else {
		let err, err1, formattedMessage;
		[err, formattedMessage] = await takeAction(payload);
		if (err) {
			console.log("error is " + err);
			let now = new Date();
			let messageSpecifics = {
				"epochtime": Math.round(now.getTime()/1000),
				"timeString": now.toString(),
				"message": err
			}
			renderTemplateFile('slackMessages/GenericResponse.json', messageSpecifics)
				.then((renderedString) => {
					let respMessage = JSON.parse(renderedString);
					console.log("Sending " + JSON.stringify(respMessage));
					respond(respMessage);
				})
				.catch((err1) => {
					//Template failed, send the raw error back instead
					let altMessage = {"text" : err};
					console.log("Sending " + JSON.stringify(altMessage));
					respond(altMessage);
				});
		} else {
			respond(formattedMessage);
		}
	}
});

/**
 * Checks for Validity of Requests from Interactive Messages
 *
 * @param {Object} payload The JSON parsed payload.
 * @return {boolean} false if it is a valid payload, a String
 * describing the error if it is a invalid payload
 */
let isRequestInvalid = (payload) => {
	let isBlockMessage = true;
	if (!payload) {
		// Button pressed had empty request
		return "Empty Payload";
	}
	if (!(payload.user && payload.user.name && payload.user.id)) {
		// No user info found
		return "User information Missing";
	}
	if (!(payload.actions && (payload.actions.length === 1))) {
		// Button Pressed didnt contain actions
		return "Invalid Payload";
	}
	let action = payload.actions[0]; // Get the action that invoked this
	if (payload.type === 'interactive_message') {
		isBlockMessage = false;
		if (!(payload.action_ts)) {
			//no timestamps in payload
			return "Missing Timestamps";
		} else {
			payload.action_tsDateObject = new Date(1000*payload.action_ts);
		}
	} else if (payload.type === 'block_actions') {
		if (!action.action_ts) {
			//no timestamps in action
			return "Missing Timestamps";
		} else {
			payload.action_tsDateObject = new Date(1000*action.action_ts);
		}
	} else {
		return "Invalid Payload Type";
	}
	if (isBlockMessage) {
		if (!action.action_id) {
			return "Missing Button Action information";
		}
	} else {
		if (!action.name) {
			return "Missing Button Action information";
		} else {
			// Create an action_id just like the block_message
			action.action_id = action.name;
		}
	}
	if (action.action_id === "sign-out-all" || action.action_id === "cancel-sign-out-all") {
		//button pressed came from sign out all command so no need to check for attendance_id
		return false;
	}
	// We need a attendance id for user sign_out, cancel_sign_out requests
	// In the older attachment message it will be sent as callback_id
	// In Block actions, it will be the action_id of the button
	if (!action.value) {
		return "Missing Attendance Id";
	}
	return false;
};

/**
 * Branching logic that calls the appropriate handler based on the slack action
 *
 * @param {Object} payload The parsed slack request payload request object.
 * @param {Object} res The HTTP response object.
 * @return {Array} array of 1-2 objects. If handler succeeds, the second object returns a slack ready
 * JSON response. Otherwise the first object, (a String) is the error description.
 *
 */
let takeAction = async function(payload) {
	let action = payload.actions[0];    //Action is already checked for
	let actionName = action.action_id;  // Decide what action to take
	let attendanceID = action.value; 	// Attendance is already checked for

	console.log("Action is " + actionName + ", id is " + attendanceID);
	if (actionName === "sign-out-all" || actionName === "cancel-sign-out-all") {
		let isSignAllOut = false;
		if (actionName === "sign-out-all") isSignAllOut = true;
		let err, formattedMessage;
		[err, formattedMessage] = await signAllOut(isSignAllOut, payload);
		return [err, formattedMessage];
	} else {
		let err, attendanceRecord;
		[err, attendanceRecord] = await getAttendanceRecord(attendanceID);
		if (err) {
			console.log(err);
			return [err]
		}

		if ((actionName === "approve")||(actionName === "reject")) {
			let isApprove = false;
			if (actionName === "approve") isApprove = true;
			let err1, formattedMessage;
			[err1, formattedMessage] = await handleApprove(isApprove, attendanceRecord, payload);
			return [err1, formattedMessage];
		} else if (actionName === "sign_out") {
			let err1, formattedMessage;
			[err1, formattedMessage] = await handleSignOut(attendanceRecord, payload);
			return [err1, formattedMessage]
		} else {
			let err1 = "Unknown Action " + actionName;
			console.log(err1);
			return [err1];
		}
	}
};

/**
 * Retrieves Attendance Record from database
 *
 * @param {String} attendanceID The attendance record ID.
 * @return {Array} array of 1-2 objects. If attendance record is found,
 * the first object is null and the second is the attendance record. If attendance record is
 * not found, only the first object, (a String) describing the error is returned.
 */
let getAttendanceRecord = async (attendanceID) => {
	
	console.log("Trying to find " + attendanceID);
	let err, attendanceRecord;
	
	try {
		attendanceRecord = await attendanceModel.findById(attendanceID);
	} catch (err) {
		return ["DB Error " + err.message];
	}
	if (!attendanceRecord){
		return ["Couldn't find attendance record " + attendanceID];
	}
	console.log("Found Attendance Record " + attendanceRecord);
	if (!(attendanceRecord.user && attendanceRecord.user.name && attendanceRecord.user.user_id)) {
		return ["User Info missing in record"];
	}
	if (!(attendanceRecord.signIn && attendanceRecord.signIn.status)) {
		return ["Attendance Status missing in record"];
	}
	return [null, attendanceRecord];
}

/**
 * Handles the "Approve" Sign In button press by the Leader
 *
 * @param {boolean} isApprove true if approve, false otherwise
 * @param {Object} record The attendance record.
 * @param {Object} payload The JSON parsed payload.
 * @return {Array} array of 1-2 objects. If there is success, the first object is null 
 * and the second is the rich message response to the interactive message. Any errors will
 * result in sending a rich message describing the error as the first object.
 */
let handleApprove = async (isApprove, record, payload) => {
 	// Dont allow the approval if the attendance sign in status is not 'SUBMITTED'
 	if (record.signIn.status != "SUBMITTED") {
 		let errMessage = "Sign in Attendance Status needs to be *SUBMITTED* and not " + record.signIn.status;
		console.log(errMessage);
		return [errMessage];
	}

	// Update the attendance record attributes
	if (isApprove) {
		record.signIn.status = "APPROVED";
	} else {
		record.signIn.status = "REJECTED";
	}
	record.signIn.approvedBy = payload.user;
	record.signIn.approveTime = payload.action_tsDateObject;
	let responseURL = record.responseURL;
	record.responseURL = null;

	// Save attendance record to Database
	let err, err1, updatedRecord;
	[err, updatedRecord] = await to(record.save());
	if (err) {
		console.log("Update failed " + err);
		return ["Status Update Failed " + err];
	}
	console.log("Updated " + JSON.stringify(updatedRecord));

	//creating a response for the person who pressed the approve button
	let resMessage = null;
	let messageSpecifics = {
		"epochtime": Math.round(payload.action_tsDateObject.getTime()/1000),
		"timeString": payload.action_tsDateObject.toString(),
	}
	let formattedSignInDate = "<!date^" + Math.round(record.signIn.time.getTime()/1000) + 
		"^{date_long_pretty} {time}| " + record.signIn.time.toString() + ">";
	messageSpecifics.message = "_" + record.user.name + "_'s (" + formattedSignInDate + ") SignIn *" + 
		record.signIn.status + "* by _" + payload.user.name + "_";
	console.log(messageSpecifics);

	[err, resMessage] = await to(renderTemplateFile('slackMessages/GenericResponse.json', messageSpecifics));
	let parsedResponse = JSON.parse(resMessage);
	console.log("Sending " + JSON.stringify(parsedResponse));

	// Make a best effort to update user's communication with the bot. No error handling needed
	if (responseURL) {
		[err1] = await to(got.post(responseURL, { json: parsedResponse }));
		if (err1) { // Dont do anything since this is a best effort call
			console.log("POST to response URL failed " + err);
		} else {
			console.log("Successfully sent message to user");
		}
	}
	return [err, parsedResponse];
}

/**
 * Handles the "Sign Out" button press by the user
 *
 * @param {Object} record The attendance record.
 * @param {Object} payload The JSON parsed payload.
 * @return {Array} array of 1-2 objects. If there is success, the first object is null 
 * and the second is the rich message response to the interactive message. Any errors will
 * result in sending a rich message describing the error as the first object.
 */
let handleSignOut = async (record, payload) => {
	//if (record.signIn.status === "CANCELLED" || record.signIn.status === "REJECTED") {
	if (record.signIn.status === "REJECTED") {
		// Allow signOut if status is not CANCELLED or REJECTED
		let returnMessage = "Can't sign you out: Your Attendance Status is " + record.signIn.status.toLowerCase();
		return [returnMessage];
	} 
	
	// Check is user has already signed out
	if (record.signOut != null && record.signOut.status != null) {
		let returnMessage = "You already signed out at " + record.signOut.time.toString();
		return [returnMessage];
	}
	
	//leadership does not need to approve signing out
	record.signOut = {
		"status": "APPROVED",
		"time": payload.action_tsDateObject
	};

	//Calculate number of minutes worked today
	let timeWorked = (Math.round(((record.signOut.time.getTime() - record.signIn.time.getTime())/1000/60)*100))/100;

	console.log("Minutes worked: " + timeWorked + " (SignIn): " + record.signIn.time.getTime() + 
		", (SignOut): " + record.signOut.time.getTime());
	record.totalMinutesWorked = timeWorked;
	
	let err, err1, updatedRecord;
	[err, updatedRecord] = await to(record.save());
	if (err) {
		let returnMessage = "Could not update attendance record";
		console.log(returnMessage);
		return [returnMessage];
	}

	console.log("Updated attendance record " + record);

	// Create response for user signout
	let messageString = record.user.name + " signed out logging *" + timeWorked + "* minutes";
	let formattedMessage;
	let messageSpecifics = {
		"message": messageString,
		"epochtime": Math.round(payload.action_tsDateObject.getTime()/1000),
		"timeString": payload.action_tsDateObject.toString()
	};
	// Create formatted message 
	[err, formattedMessage] = await to(renderTemplateFile('slackMessages/GenericResponse.json', messageSpecifics));
	// If formatting fails, we need a dummy message to send back
	if (err) {
		console.log("Messsage Formatting failed " + err);
		formattedMessage = {"text": "User Signed Out"};
	}
	let JSONformattedMessage = JSON.parse(formattedMessage);
	// If there was no error, Post message to leadership channel
	if (!err) {
		[err1] = await to(webhook.send(JSONformattedMessage));
		// Ignore the error if there is an error
		if (err1) {
			console.log("Could not send message to leadership channel " + err1);
		}
	}

	console.log("Send back to user: " + JSON.stringify(JSONformattedMessage));
	return [err, JSONformattedMessage];
}

/**
 * Handles the "Sign All Users Out" button press by the Leader or its cancellation
 *
 * @param {action} isSignAllOut true unless it is a cancel request
 * @param {Object} payload The JSON parsed payload.
 * @return {Array} array of 1-2 objects. If there is success, the first object is null 
 * and the second is a rich message response to the interactive message. Any errors will
 * result in sending a rich message describing the error in the first object.
 */
let signAllOut = async (isSignAllOut, payload) => {
	let actionStatus = null;
	if (isSignAllOut) { // Sign all users out
		// Add a signOut object to all attendances with APPROVED and SUBMITTED statuses
		let attendances, err;

		[err, attendances] = await to(attendanceModel.updateMany(
			{ "signOut": null, "signIn.status": {$in: ["APPROVED", "SUBMITTED"]} },
			{ $set: {"signOut.status": "APPROVED", "signOut.time": payload.action_tsDateObject}	},
			{ multi: true})
		);
		if (err) {
			console.error("Could not update " + err);
			return ["Could not update " + err];
		}

		console.log("Updates: " + JSON.stringify(attendances));

		if (attendances && attendances.ok) { // The update worked!
			actionStatus = "Signed Out *" + attendances.nModified + "* users";
		} else {
			actionStatus = "Unknown result";
		}
	} else { // Cancel Action, do nothing
		actionStatus = "_sign-all-out_ Cancelled";
	}

	const now = new Date();
	let messageSpecifics = {
		"message": actionStatus,
		"epochtime": Math.round(now.getTime()/1000),
		"timeString": now.toString()
	};

	[err, formattedMessage] = await to(renderTemplateFile('slackMessages/GenericResponse.json', messageSpecifics));
	return [err, JSON.parse(formattedMessage)];
}

module.exports = slackInteractions.expressMiddleware();