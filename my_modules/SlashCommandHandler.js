/**
 * Summary. This file contains the main function that handles a slack slash
 * commands for the Attendance Application.
 *
 * Description. The commands supported by the Attendance App include /signin,
 * /sign-all-out and /get-summary
 *
 * @link   
 * @file   
 * @author Varun Bhajekar
 * @since  1.0.1
 */

const attendanceModel = require('../lib/mongoModels.js').ATTENDANCE_MODEL;
const { renderTemplateFile } = require('template-file');

const { IncomingWebhook } = require('@slack/webhook');
const slackWebhook = new IncomingWebhook(process.env.LEADERSHIP_WEBHOOK);

const signature = require("./verifySlackSignature");

const to  = require('await-to-js').default;

const { WebClient } = require('@slack/web-api');
const web = new WebClient(process.env.BOT_TOKEN);

/**
 * Main Slash Command handler that handles all slash command requests from slack for
 * the attendance app. It is designed to be asynchronous
 *
 * @param {Object} req The HTTP Request object.
 * @param {Object} res The HTTP response object.
 * @return {null} Nothing.
 *
 */
let handleSlashCommand = async(req, res) => {

	// Verify that message is from slack
	// If it gets through it is guaranteed to have a body
	if (!signature.isVerified(req)) {
    	console.log('Verification token mismatch');
    	return res.status(404).send();
  	}

  	let payload = req.body;
	payload.ts = new Date(); // Add a time stamp that we'll use later
	console.log("Payload is " + JSON.stringify(payload));

	// Check if this is a payload we can work with, otherwise send a 404
	//
	if (!(payload.command && payload.channel_id && payload.user_id && payload.user_name)) {
		console.log('Invalid Payload');
    	return res.status(404).send();
	}

	// Depending on the Command, run the proper handler
	//
	let err, err1, formattedResponse;

	//
	// The command selection handler that follows either returns an error or a formatted slack response
	//

	switch(payload.command) {
		case '/signin':
			console.log("The sign-in handler starts here");
			[err, formattedResponse] = await handleSignIn(payload);
			break;
		case '/sign-all-out':
			console.log("The sign-all-out handler starts here");
			[err, formattedResponse] = await handleSignAllOut(payload);
			break;
		case '/get-summary':
			console.log("The get-summary handler starts here");
			[err, formattedResponse] = await handleGetSummary(payload);
			break;
		default:
			console.log("Unknown Command " + payload.command);
			return res.status(404).send();
			break;
	}

	// If the handler throws an error, use Generic Template to send response back
	//
	if (err) {
		console.log("error is " + err);
		let now = new Date();
		let messageSpecifics = {
			"epochtime": Math.round(now.getTime()/1000),
			"timeString": now.toString(),
			"message": err
		}
		renderTemplateFile('slackMessages/GenericResponse.json', messageSpecifics)
			.then((renderedString) => {
				let respMessage = JSON.parse(renderedString);
				console.log("Sending " + JSON.stringify(respMessage));
				res.send(respMessage);
			})
			.catch((err1) => {
				//Template failed, send the raw error instead
				let altMessage = {"text" : err};
				console.log("Sending " + JSON.stringify(altMessage));
				res.send(altMessage);
			});
	} else {
		// All good! Send the handler's output (response) back to the user!
		//
		res.send(formattedResponse);
	}
};

/**
 * Handles the "/signin" request
 *
 * @param {Object} payload The JSON parsed request payload.
 * @return {Array} array of 1-2 objects. If there is success, the first object is null 
 * and the second is the rich message response to be sent to the invoker of the slash command.
 * Any errors will be in the first object returned.
 */
let handleSignIn = async (payload) => {
	// First check if the command was issued as a direct message to the ATTENDANCE_BOT
	//
	let err, APIresults;

	[err, APIresults] = await to (web.conversations.members({channel: payload.channel_id}));
	if ((err) /*|| (!APIresults)*/) {
		console.log("Slack Web API call failed: " + JSON.stringify(err));
		if (err.data && err.data.error === "channel_not_found") {
			return["Please */signin* in a Direct Message with the Attendance BOT *ONLY*"];
		} 
		return ["Slack Web API call failed"];
	}
	console.log("WebAPI results " + JSON.stringify(APIresults));
	if (!APIresults || (!APIresults.ok && !(APIresults.members))) {
		console.log("Unexpected results from API Call");
		return ["Slack Web API call failed"];
	}
	let members = APIresults.members;
	// Is this a direct message between the BOT and the user? There should be two members
	//
	if (!(
			(members.length == 2) && 
			(members.find(o => o === payload.user_id)) && 
			(members.find(o => o === process.env.BOT_ID))
	     )
	) {
		return ["Please */signin* in a Direct Message with the Attendance BOT *ONLY*"];
	}

	// Check if user has an active sign in already
	//
	let signInsQuery = {
		"user.user_id": payload.user_id, 
		"signOut": null, 
		"signIn.status": {$in: ["APPROVED", "SUBMITTED"]}
	} // Look for all logins that don't have a sign out yet and are in APPROVED or SUBMITTED status

	let attendances;
	[err, attendances] = await to(attendanceModel.find(signInsQuery));
	if ((err) || !(attendances)) {
		console.log("Error in getting signins " + err);
		return["Cant retrieve existing logins"];
	}
	if (attendances.length > 0) {
		console.log("Got Existing SignIns " + JSON.stringify(attendances));
		// Assume there is only one for simplicity
		let returnMessage = "You already have an active SignIn *" + 
			attendances[0].signIn.time.toLocaleString() + "*. Sign Out first";
		return [returnMessage];
	}

	// Now we are ready to add an attendance record
	//
	let NewAttendanceRecord = new attendanceModel({
		user: {name: payload.user_name, user_id: payload.user_id},
		signIn: {time: payload.ts, status: "SUBMITTED"},
		responseURL: payload.response_url
	});

	let insertedRecord;
	[err, insertedRecord] = await to(NewAttendanceRecord.save());

	if (err) {
		console.log("DB Error " + err);
		return ["Couldn't create Attendance Record"];
	}
	console.log("Inserted Attendance Record: " + JSON.stringify(insertedRecord));

	// Inform leadership
	//
	let leadershiNotification = await informLeadership(payload, insertedRecord.id);
	if (!leadershiNotification) {
		// Notification to Leadership has failed. User's signin cannot be approved.
		return ["Your signin cannot be approved. Please contact leadership directly"];
	}

	let messageSpecifics = {
		"epochtime": Math.round(payload.ts.getTime()/1000),
		"timeString": new Date(payload.ts).toString(),
		"cbId": insertedRecord.id
	}

	// Create Acknowledgement to send back to user
	//
	let renderedString;
	[err,renderedString] = await to(renderTemplateFile('slackMessages/SignInResponse.json', messageSpecifics));
	let respMessage = JSON.parse(renderedString);
	console.log("Sending " + JSON.stringify(respMessage));
	return [null, respMessage];
}

/**
 * Handles the "/get-summary" request
 *
 * @param {Object} payload The JSON parsed request payload.
 * @return {Array} array of 1-2 objects. If there is success, the first object is null 
 * and the second is the rich message response to be sent to the invoker of the slash command.
 * Any errors will be in the first object returned.
 */
let handleGetSummary = async (payload) => {
	//make sure command was requested in leadership channel
	if (payload.channel_id != process.env.LEADERSHIP_CHANNEL_ID) {
		return ["*/get-summary* is only available for members in the leadership channel"];
	}
	// Create a aggregation request that summarizes logins from the database and total hours worked
	let summaryModel = [
		// Need all records that have approved sign-ins and sign-outs
		{$match: {"signIn.status": "APPROVED", "signOut.status": "APPROVED"}},
		// Need to group them by user.name, count the logins, compute the total hours
		{
			$group: {
				"_id": "$user.name",
				"totalSignIns": {"$sum": 1}, // Get a count of sign-ins
				"totalHours": {
					"$sum": { // Add all hours
				 		"$divide": [ // Divide time by 3600000 to get time in minutes
				 			{
				 				"$subtract" : ["$signOut.time", "$signIn.time"] // signOut - signIn
				 			}, 
				 			3600000
				 		]
				 	}
				}
			}
		}
	]
	let err, summary;
	[err, summary] = await to(attendanceModel.aggregate(summaryModel).exec());
	if (err || !summary) {
		console.log("DB Summary Request failed " + err);
		return["DB Summary Request failed"];
	}

	if (summary.length <= 0) {
		console.log("No Attendance Records Found");
		return["No Attendance Records Found"];
	}

	console.log("Retrieved " + JSON.stringify(summary));

	// Format the responses.
	let totalUsers = summary.length;
	let sumUsers = "";
	let sumHours = "";
	for (let i=0; i<totalUsers; i++) {
		let loginLength = summary[i]._id.length;
		sumUsers += '\\n'; // Add a new line
		if (loginLength > 12) { 
			// Truncate user names to 12 max for slack purposes
			sumUsers += summary[i]._id.substring(0,7) + "..." + 
						summary[i]._id.substring(summary[i]._id.length-2,summary[i]._id.length);
		} else {
			sumUsers += summary[i]._id;
		}
		sumHours += '\\n' + summary[i].totalHours.toFixed(2) + " (" + summary[i].totalSignIns + ")";
	}

	var messageSpecifics = {
		"hours": sumHours,
		"users": sumUsers,
		"epochtime": Math.round(payload.ts.getTime()/1000),
		"timeString": new Date(payload.ts).toString()
	};

	[err, renderedString] = await to(renderTemplateFile('slackMessages/AttendanceSummary.json', messageSpecifics));
	if (err) { // Template failed, send raw data back
		return [null, JSON.stringify(summary)];
	}

	let formattedMessage = JSON.parse(renderedString);
	console.log("Sending to User " + JSON.stringify(formattedMessage));
	return [null, formattedMessage];
}

/**
 * Handles the "/sign-all-out" request
 *
 * @param {Object} payload The JSON parsed request payload.
 * @return {Array} array of 1-2 objects. If there is success, the first object is null 
 * and the second is the rich message response to be sent to the invoker of the slash command.
 * Any errors will be in the first object returned.
 */
let handleSignAllOut = async (payload) => {
	//make sure command was requested in leadership channel
	if (payload.channel_id != process.env.LEADERSHIP_CHANNEL_ID) {
		return ["*/sign-all-out* is only available for members in the leadership channel"];
	}

	let aggregateSignInModel = [
		{
			// Get all records with no SignOuts and signIn statuses APPROVED or SUBMITTED
			$match: { "signOut": null, "signIn.status": {$in: ["APPROVED", "SUBMITTED"]}}
		},
		{
			// Group results by SUBMITTED and APPROVED, provide the "count"
			$group: {_id: "$signIn.status", "count": {"$sum": 1}}
		}
	]

	let err, summary;

	[err, summary] = await to(attendanceModel.aggregate(aggregateSignInModel).exec());
	if (err || !summary) {
		console.log("DB Summary Request failed " + err);
		return["DB Summary Request failed"];
	}

	if (summary.length <= 0) {
		console.log("All users have already signed out");
		return["No users need to be signed out at this time"];
	}

	console.log("Retrieved " + JSON.stringify(summary));

	// Get the submitted and approved user count that can be signed out
	//
	let submitted = 0;
	let approved = 0;

	let subSummary = summary.find(o => o._id === "SUBMITTED");
	if (subSummary) submitted = subSummary.count;

	let appSummary = summary.find(o => o._id === "APPROVED");
	if (appSummary) approved = appSummary.count;

	// Format message to send back
	//
	let messageSpecifics = {
		"submitted": submitted,
		"approved": approved,
		"total": submitted + approved,
		"epochtime": Math.round(payload.ts.getTime()/1000),
		"timeString": new Date(payload.ts).toString()
	};

	[err, renderedString] = await to(renderTemplateFile('slackMessages/SignOutAllTemplate.json', messageSpecifics));
	if (err) { // Template failed, send raw data back
		return [null, JSON.stringify(summary)];
	}
	let formattedMessage = JSON.parse(renderedString);
	console.log("Sending to User " + JSON.stringify(formattedMessage));
	return [null, formattedMessage];
}


/**
 * Sends a message to the Leadership channel reagrding a new login
 *
 * @param {Object} payload The JSON parsed request payload.
 * @param {String} attendanceId Id of the new attendance record.
 * @return {boolean} true if successful, else false.
 */

let informLeadership = async (payload, attendanceId) => {
	let messageSpecifics = {
		"user": payload.user_name,
		"epochtime": Math.round(payload.ts.getTime()/1000),
		"timeString": new Date(payload.ts).toString(),
		"cbId" : attendanceId
	}
	let err, renderedString;
	[err, renderedString] = await to(renderTemplateFile('slackMessages/LeaderApprovalTemplate.json', messageSpecifics));
	if (err) {
		console.log("Couldn't create leadership message " + err);
		return false;
	}
	let leadershipMessage = JSON.parse(renderedString);
	// Send Message via webhook
	//
	[err] = await to(slackWebhook.send(leadershipMessage));
	if (err) {
		console.log("Couldn't send to Leadership Channel " + err);
		return false;
	}
	console.log("Success sending to Leadership " + JSON.stringify(leadershipMessage));
	return true;
}

module.exports = handleSlashCommand;